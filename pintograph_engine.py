"""
	Copyright (c) 2016 Arttu Ylä-Sahra

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import numpy as np

"""
	Pintograph - main class for the pintograph generator
"""

class Pintograph:

	def __init__(self, r1, r1_phase_per_step, r2, r2_phase_per_step, circle_distance, left_rod_lngth, right_rod_lngth, lft_radii_minimum_factor = 1, lft_radii_degrade_percentage_per_step = 0, rght_radii_minimum_factor = 1, rght_radii_degrade_percentage_per_step = 0):
		"""
			Initializes a Pintograph class. Certain basic parameters must be given to establish a working generator
			
			Keyword arguments:
			r1 -- Radius for the left circle
			r1_phase_per_step -- A multipler; how many radians would one step result in?
			lft_radii_minimum_factor -- The factor for the minimum length of the radius when completely shortened (e.g if assigned radius is 10, and the factor is 0.5, then the minimum length is 5)
			lft_radii_degrade_percentage_per_step -- Considering the difference between the minimum length and start length, how many % of it is removed from the rod on each step?

			r2 -- Radius for the right circle
			r2_phase_per_step -- A multipler; how many radians would one step result in?
			rght_radii_minimum_factor -- The factor for the minimum length of the radius when completely shortened (e.g if assigned radius is 10, and the factor is 0.5, then the minimum length is 5)
			rght_radii_degrade_percentage_per_step -- Considering the difference between the minimum length and start length, how many % of it is removed from the rod on each step?
			
			circle_distance -- Distance between circles
			
			left_rod_lngth -- The length of the left rod
			
			right_rod_lngth -- The length of the left rod
	
			
			--
			
			NOTE - simply calling an initializer is not enough, a scaling is required aswell for most purposes.
		"""
		self.r1 = r1
		self.r1_phase_per_step = r1_phase_per_step

		self.r2 = r2
		self.r2_phase_per_step = r2_phase_per_step

		self.circle_distance = circle_distance
		self.left_rod_lngth = left_rod_lngth
		self.right_rod_lngth = right_rod_lngth

		self.lft_radii_minimum_factor = lft_radii_minimum_factor
		self.lft_radii_degrade_percentage_per_step = lft_radii_degrade_percentage_per_step

		self.rght_radii_minimum_factor = rght_radii_minimum_factor
		self.rght_radii_degrade_percentage_per_step = rght_radii_degrade_percentage_per_step

		"""
			Extended values, which are not necessarily required but can be useful.
			
			r1_initial_phase -- Initial phase of the left circle
			r2_initial_phase -- Initial phase of the right circle
			
			step_x_swing -- How much swing is applied to the X coordinate over time; the amount of swing is controlled by a sine function in relation to the current step
			step_x_swing_initial_phase -- Initial phase for the sine function
			step_x_swing_phase_per_step -- A multipler; how many radians would one step result in?
			step_x_swing_decays_in_steps -- In how many steps the swinging stops totally?
		"""
		self.r1_initial_phase = 0
		self.r2_initial_phase = 0

		self.step_x_swing = 0
		self.step_x_swing_initial_phase = 0
		self.step_x_swing_phase_per_step = 0
		self.step_x_swing_decays_in_steps = 0

	def set_initial_phases(self, i1, i2):
		"""
			Sets initial phases as presented and documented in __init__()
		"""
		self.r1_initial_phase = i1
		self.r2_initial_phase = i2

	def set_x_swing(self, swing_amount, ip, phase_per_step, decay):
		"""
			Sets X swing properties as presented and documented in __init__
			
			Keyword arguments and their equivalence to ones documented in init:
				swing_amount -- step_x_swing
				ip -- step_x_swing_initial_phase
				phase_per_step -- step_x_swing_phase_per_step
				decay -- step_x_swing_decays_in_steps
		"""
		self.step_x_swing = swing_amount
		self.step_x_swing_initial_phase = ip
		self.step_x_swing_phase_per_step = phase_per_step
		self.step_x_swing_decays_in_steps = decay

	def apply_step_swing(self, xy, step):
		"""
			Applies the swing to a given XY coordinate pair, using the given step number
		"""
		#No swing, no change
		if (self.step_x_swing == 0):
			return xy

		#Let's assume that we have valid values for the swing decay
		decay_percentage = step / float(self.step_x_swing_decays_in_steps)

		#1.0 or over, totally degraded
		if (decay_percentage >= 1):
			return xy

		return xy + (np.array([np.sin(self.step_x_swing_initial_phase + step*self.step_x_swing_phase_per_step) * self.step_x_swing, 0]) * (1.0-decay_percentage))
		 
	def actual_phases_for_step(self, step):
		"""
			Returns the true phases for the circles after generator's step-dependent properties have been calculated
		"""

		return np.array([self.r1_initial_phase + step*self.r1_phase_per_step, self.r2_initial_phase + step*self.r2_phase_per_step])

	def actual_radii_lengths_for_step(self, step):
		"""
			Returns actual radii for the circles, when step-dependent properties have been applied
		"""
		left_radius_difference_length = self.r1 * (1.0 - self.lft_radii_minimum_factor)
		right_radius_difference_length = self.r2 * (1.0 - self.rght_radii_minimum_factor)

		left_step_percentage = max(0.0, min(1.0, self.lft_radii_degrade_percentage_per_step * step))
		right_step_percentage = max(0.0, min(1.0, self.rght_radii_degrade_percentage_per_step * step))

		#Assemble an array of differences calculated with factors applying for the current step
		actual_diffs = np.prod([[left_radius_difference_length, right_radius_difference_length], [left_step_percentage, right_step_percentage]], axis=0)

		#Return the adjusted radii
		return np.array([self.r1, self.r2]) - actual_diffs

	def normalized_direction_to_point(from_point, to_point):
		"""
			Returns a normalized direction vector between points
			
			Keyword arguments:
				from_point -- Where the vector begins
				to_point -- Where the vector shall point to
		"""
		diff = np.array(to_point) - np.array(from_point)
		distance = Pintograph.distance_between_points(from_point, to_point)

		return diff / distance

	def distance_between_points(point1, point2):
		"""
			Returns the distance between two points
		"""
		#Numpy makes this a fairly trivial exercise
		return np.linalg.norm(np.array(point1)-np.array(point2))


	def calculate_raw_step(self, step):
		"""
			Calculates the raw XY coordinate for a given step
		"""
		#Centers of circles
		pr1 = np.array([-1.0 * (self.circle_distance), 0])
		pr2 = np.array([1.0 * (self.circle_distance), 0])

		#Actual phases
		actual_phases = self.actual_phases_for_step(step)

		#Actual radius lengths for circles
		actual_circle_radii_lengths = self.actual_radii_lengths_for_step(step)

		#Calculate the starting points for rods
		p1 = pr1 + np.array([actual_circle_radii_lengths[0] * np.cos(actual_phases[0]), actual_circle_radii_lengths[0] * np.sin(actual_phases[0])])
		p2 = pr2 + np.array([-1.0 * (actual_circle_radii_lengths[1] * np.cos(actual_phases[1])), actual_circle_radii_lengths[1] * np.sin(actual_phases[1])])

		#Calculate A, and then the angle CA
		a = Pintograph.distance_between_points(p1,p2)

		c_up = (self.left_rod_lngth ** 2) + (a * a) - (self.right_rod_lngth ** 2)
		c_div = 2.0 * (self.left_rod_lngth) * a

		angle_c = np.arccos(c_up / c_div)

		if (np.isnan(angle_c)):
			print("PROBLEM: angle was a NaN. Actual radii: " + str(actual_circle_radii_lengths) + ", actual angles in radians: " + str(actual_phases))
			
			print("Original r1: " + str(self.r1))
			print("Original r2: " + str(self.r2))
			
			print("Pr1: " + str(pr1))
			print("Pr2: " + str(pr2))

			print("P1: " + str(p1))
			print("P2: " + str(p2))

			print("C: " + str(self.left_rod_lngth))
			print("B: " + str(self.right_rod_lngth))
			print("A: " + str(a))


		#Direction vector in relation to A
		a_dir_vector = Pintograph.normalized_direction_to_point(p1,p2)
		c_dir_vector = np.prod([a_dir_vector, [np.cos(angle_c), np.sin(angle_c)]], axis=0)

		#The Point (pPen)
		return p1 + (self.left_rod_lngth * c_dir_vector)

	def calculate_adjusted_step(self, step):
		"""
			Calculates an adjusted step: centered and scaled to given bounds.
			NOTE: This function may and most likely WILL return negative coordinates. You need to factor that in when using this library
		"""
		raw_step = self.calculate_raw_step(step)
		centered_swung = self.apply_step_swing(raw_step - self.center_point, step)

		return centered_swung * self.scale_factor

	def quick_scale(self):
		"""
			Quickscale: 0 to 1000 in step values, with 0.8 "steps" (0, 0.8, 1.6, ... 999.2); the picture will be 1500x1500px with a 50px edge of blank
		"""
		self.automatic_scale_and_center(1000, 0.8, 1500, 50)

	def automatic_scale_and_center(self, steps, scale, new_side, edge):
		"""
			Sets automatic scaling values for the generator. MUST be called before asking for scaled steps, because otherwise it will crash due to a lack of set properties
			
			Keyword arguments:
				steps -- Maximum step value for the raw step method
				scale -- How large is a "step"	between step values?
				new_side -- The length of a side: any given X or Y value will obey the condition "abs(v) <= (new_side/2)-edge"
				edge -- How much of a blank will be left on the edges of the picture?
		"""
			
		#First, let's calculate an average for the steps, so we know where to center
		raw_steps = []
		step_vars = np.append(np.arange(0, steps, scale), [steps])
		for step_var in step_vars:
			raw_steps.append(self.calculate_raw_step(step_var))

		average_location = np.average(raw_steps, axis=0)
		#Save it
		self.center_point = average_location
	
		#Now, next step. Let's copy the centered values to a new array, and then factor in the X-swing so we can factor it in the total area required
		centered_steps = []
		for i in range(len(raw_steps)):
			raw_step = raw_steps[i]
			centered_steps.append(self.apply_step_swing(raw_step - average_location, step_vars[i]))
		
		#Let's find the farthest locations, and calculate the factor required using them
		highest_encountered_x = 0
		highest_encountered_y = 0

		for stp in centered_steps:
			if (abs(stp[0]) > highest_encountered_x):
				highest_encountered_x = abs(stp[0])
			if (abs(stp[1]) > highest_encountered_y):
				highest_encountered_y = abs(stp[1])

		#Set an XY boundary as an instance variable
		self.xy_boundary = new_side

		#We want a linear scale, so we must take the farthest X or Y position, and use that as a base to our scaling

		maximal_distance = max(highest_encountered_x, highest_encountered_y)

		#Set the scale factor..
		self.scale_factor =	 float((new_side / 2.0)-edge) / maximal_distance

		#And we're done!
