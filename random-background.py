"""
	Copyright (c) 2016 Arttu Ylä-Sahra

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

"""
	random-background.py - A simple generator script using the Pintograph library for generating images. Each time it is a run, an image with a random filename is generated
"""

from pintograph_engine import Pintograph
from PIL import Image, ImageDraw
import random
import math

#How large the picture shall be?
BACKGROUND_SIZE = 4500

#Base steps: this amount will be ran for autodetection of position, and 10x that for actual rendering
BASE_STEPS = 1000

def realign_cords(xy):
	"""
		As the Pintograph library generates coordinates around an arbitrary center point, we need to convert the coordinates to a type suitable for images - aka starting from zero
	"""
	global BACKGROUND_SIZE

	return [int(xy[0] + (BACKGROUND_SIZE / 2)), int((BACKGROUND_SIZE-1) - (xy[1] + BACKGROUND_SIZE / 2))]

#Initialize the image library
base_img = Image.new("RGBA", (BACKGROUND_SIZE, BACKGROUND_SIZE), (255,255,255,255))
base_draw = ImageDraw.Draw(base_img)

#Initialize the variables required for the Pintograph library
random.seed()
#How long the rods will be?
rod_length = 50 + random.randint(-20,20)

#One full phase is 2pi, and we want it to complete a full circle in 10 steps, so.. 
phases_per_step = (2*math.pi) / 10

#Starting angle at approximately 173 to 287 degrees for both
initial_phase = (4.01 - 1) + (random.random() * 2)

#The radii for the circles
radii = 10

pinto = Pintograph(r1=radii,
				   r1_phase_per_step = phases_per_step,
				   r2=radii,
				   r2_phase_per_step = phases_per_step - 0.02 + (random.random() * 0.04),
				   circle_distance = ((rod_length * 2) * (2.0/7.0)),
				   left_rod_lngth = rod_length,
				   right_rod_lngth = rod_length + int((random.random() * 5)),

				   lft_radii_minimum_factor = 0.01,
				   lft_radii_degrade_percentage_per_step = 0.0010752 + (random.random() / 10000),

				   rght_radii_minimum_factor = 0.01,
				   rght_radii_degrade_percentage_per_step = 0.0010751 + (random.random() / 10000),
				   )


pinto.set_initial_phases(initial_phase - 0.03 + (random.random() * 0.06), initial_phase)
pinto.set_x_swing((rod_length*2)/7, 0, phases_per_step, BASE_STEPS * 0.85)

#Run a rough scan and scale
pinto.automatic_scale_and_center(BASE_STEPS, 0.75, BACKGROUND_SIZE, 2)	

base_pos = realign_cords(pinto.calculate_adjusted_step(0))

for i in range(BASE_STEPS * 10):
	new_pos = realign_cords(pinto.calculate_adjusted_step(i / 10.0))
	#Simulate opacity; as actually implementing it would be slightly complicated, let's assume the line is darker when farther in the steps
	c = int(200 - (i / (BASE_STEPS*10.0))*180)
	base_draw.line([base_pos[0], base_pos[1], new_pos[0], new_pos[1]], (c,c,c,c), 1)
	base_pos = new_pos

base_img.save("bg-"+str(random.randint(0,10000000000))+".png")
