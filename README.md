# What's this?

This is a practical implementation of a Pintograph, [as described in the related article](http://arttuys.fi/projects/pintograph).

# To run

Simply copy all files to a folder, and run _random\_background.py_. Dependencies required include Pillow and Numpy. Once ran, a random image is generated to the same folder, using the library located in _pintograph\_engine.py_
